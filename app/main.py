import asyncio
import aio_pika
import json
from config import MQ_URL, settings
from funcs import database, send_confirmation_email, update_user_music_data
from schema import InnerUser, UserToken

func_mapping = {
    "send_confirm_message": {'func': send_confirmation_email, 'args': [InnerUser]},
    "update_user_music_data": {'func': update_user_music_data, 'args': [UserToken]}
}


async def on_message(message: aio_pika.IncomingMessage):
    async with message.process():
        body = json.loads(message.body)
        action = body.pop('action', None)
        if action:
            func_dict = func_mapping[action]
            func = func_dict['func']
            args = [schema(**body[schema.__name__]) for schema in func_dict['args']]
            await func(*args)


async def main():
    mq_connection = await aio_pika.connect_robust(MQ_URL)
    channel = await mq_connection.channel()
    user_exchange = await channel.declare_exchange("users", aio_pika.ExchangeType.DIRECT)
    queue = await channel.declare_queue(durable=True)
    await database.connect()
    await queue.bind(user_exchange, routing_key=settings.user_routing_key)
    await queue.consume(on_message)


if __name__ == "__main__":
    print("WAITING FOR TASKS")
    loop = asyncio.get_event_loop()
    loop.create_task(main())
    loop.run_forever()


