from pydantic import BaseModel, EmailStr, UUID4


class User(BaseModel):
    username: str
    password: str
    email: EmailStr


class InnerUser(User):
    id: int


class Confirmation(BaseModel):
    confirmation_id: int
    user_id: int
    uuid: UUID4


class UserToken(BaseModel):
    token_id: int