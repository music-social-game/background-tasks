import uuid
import databases
import smtplib
import asyncio
import pickle
import json
import tekore as tk
from functools import partial
from config import settings, DATABASE_URL
from schema import InnerUser, UserToken
from models import Confirmation, Token, MusicData
from sqlalchemy import insert, select, update

database = databases.Database(DATABASE_URL)
spotify = tk.Spotify()
cred = tk.Credentials(settings.spotify_client_id,
                      settings.spotify_secret,
                      sender=tk.AsyncSender(),
                      asynchronous=True)


async def send_confirmation_email(user: InnerUser):
    user_uuid = uuid.uuid4()
    query = insert(Confirmation).values(user_id=user.id,
                                        uuid=user_uuid)
    await database.execute(query)
    message = f'Перейди по ссылке для подтвержения аккаунта {settings.host}confirm/{user_uuid}'
    loop = asyncio.get_running_loop()
    loop.run_in_executor(None, partial(send_mail, user.email, message))


def send_mail(email, message):
    try:
        server = smtplib.SMTP(f'{settings.smtp_host}', settings.smtp_port)
        server.ehlo()
        server.starttls()
        server.login(settings.smtp_user, settings.smtp_password)
        server.sendmail(settings.from_addr, email, message)
        server.quit()
    except smtplib.SMTPAuthenticationError:
        print(message)


def get_artists(data):
    return [artist['name'] for artist in data]


async def update_user_music_data(usertoken: UserToken):
    await database.connect()
    query = select(Token).where(Token.id == usertoken.token_id)
    token = await database.fetch_one(query)
    if token is None:
        return
    user_id = token['user_id']
    token = pickle.loads(token['spotify_token'])
    if token.is_expiring:
        token = await cred.refresh(token)
        query = update(Token).where(Token.id == usertoken.token_id).values(spotify_token=pickle.dumps(token))
        await database.execute(query)
    with spotify.token_as(token):
        top_tracks = spotify.current_user_top_tracks(limit=50).items.json()
        data = json.loads(top_tracks)
        data = [{'uri': d['uri'],
                 'artists': get_artists(d['artists']),
                 'title': d['name'],
                 'preview_url': d['preview_url']} for d in data]
        query = insert(MusicData).values(user_id=user_id,
                             spotify_data=data)
        await database.execute(query)

